##
## Makefile for rt in /home/boguta_m/rendu/mul_2013_raytracer
##
## Made by Maxime Boguta
## Login   <boguta_m@epitech.net>
##
## Started on  Fri Jun  6 00:41:43 2014 Maxime Boguta
## Last update Wed Jun 11 16:16:33 2014 Maxime Boguta
## Last update Sat Jun  7 05:11:43 2014 camill_n
##

SRCS		=	src/main.c \
			src/alloc.c \
			src/init.c \
			src/flags.c \
			src/flags2.c \
			src/errors.c \
			src/output.c \
			src/lists.c \
			src/hook.c \
			src/scene.c \
			src/utils.c \
			src/put_pixel.c \
			src/calc/calc.c \
			src/calc/antialiasing.c \
			src/calc/eye.c \
			src/calc/shadows.c \
			src/calc/colors.c \
			src/calc/cast.c \
			src/calc/checks.c \
			src/calc/reflex.c \
			src/calc/vectors.c \
			src/calc/norm.c \
			src/calc/factors.c \
			src/calc/rotate.c \
			src/parser/parse_utils.c \
			src/parser/parse.c \
			src/parser/init.c \
			src/parser/parse_entity.c \
			src/parser/parse_light.c \
			src/obj/sphere.c \
			src/obj/texture.c \
			src/obj/cone.c \
			src/obj/plan.c \
			src/obj/tore.c \
			src/obj/cubet.c \
			src/obj/cylindre.c \
			src/equat4.c \
			src/equat42.c \
			src/network/util.c \
			src/network/listen.c \
			src/network/util2.c \
			src/network/send.c \

SRCS_LIB 	= 	lib/src/my_printf.c \
			lib/src/my_putchar.c \
			lib/src/my_putstr.c \
			lib/src/my_getnbr.c \
			lib/src/getnbr.c \
			lib/src/error.c \
			lib/src/my_put_bin.c \
			lib/src/wordtab2.c \
			lib/src/wordtab.c \
			lib/src/my_getnbr.c \
			lib/src/my_put_hexa.c \
			lib/src/complement.c \
			lib/src/change_base.c \
			lib/src/my_put_nbr.c \
			lib/src/str.c \
			lib/src/mem.c \
			lib/src/my_realloc.c \
			lib/src/func_tab.c \
			lib/src/get_next_line.c \
			lib/src/my_revstr.c \

SRCX		=  	lib/minilibx/mlx_init.c \
			lib/minilibx/mlx_new_window.c \
			lib/minilibx/mlx_pixel_put.c \
			lib/minilibx/mlx_loop.c \
			lib/minilibx/mlx_mouse_hook.c \
			lib/minilibx/mlx_key_hook.c \
			lib/minilibx/mlx_expose_hook.c \
			lib/minilibx/mlx_loop_hook.c \
			lib/minilibx/mlx_int_anti_resize_win.c \
			lib/minilibx/mlx_int_do_nothing.c \
			lib/minilibx/mlx_int_wait_first_expose.c \
			lib/minilibx/mlx_int_get_visual.c \
			lib/minilibx/mlx_flush_event.c \
			lib/minilibx/mlx_string_put.c \
			lib/minilibx/mlx_new_image.c \
			lib/minilibx/mlx_get_data_addr.c \
			lib/minilibx/mlx_put_image_to_window.c \
			lib/minilibx/mlx_get_color_value.c \
			lib/minilibx/mlx_clear_window.c \
			lib/minilibx/mlx_xpm.c \
			lib/minilibx/mlx_int_str_to_wordtab.c \
			lib/minilibx/mlx_destroy_window.c \
			lib/minilibx/mlx_int_param_event.c \
			lib/minilibx/mlx_int_set_win_event_mask.c \
			lib/minilibx/mlx_hook.c \
			lib/minilibx/mlx_rgb.c

RM	= rm -f

NAME	= rt

AR	= ar rc

CC	= cc -O3

LIB_PATH = lib/

OBJS	= $(SRCS:.c=.o)

OBJS_LIB = $(SRCS_LIB:.c=.o)

OBJS_LIBX = $(SRCX:.c=.o)

LIB = libmy

LIBX = libmlx.a

CFLAGS = -I./includes -I./lib/includes

all: 	$(LIB) $(NAME)

.c.o:
	$(CC) $(CFLAGS) -c $< -o $(<:.c=.o)

$(LIB): $(OBJS_LIB)
	$(AR) includes/libmy.a lib/src/*.o
	ranlib includes/libmy.a

$(LIBX):
	make -C lib/minilibx/

$(NAME): $(OBJS)
	 $(CC) $(OBJS) -o $(NAME) -lmy -L./includes -L./lib/minilibx -lmlx -L/usr/lib64 -lm -L/usr/lib64/X11 -lXext -lX11 -lpthread

clean:
	$(RM) $(OBJS) $(OBJS_LIB)

fclean:	clean
	$(RM) $(NAME) $(DBG) libmy.a

re: fclean all
