/*
** errors.c for mysh in /home/camill_n/rendu/PSU_2013_minishell2
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Mon Feb 10 20:51:32 2014 camill_n
** Last update Mon Jun  2 23:36:31 2014 Maxime Boguta
*/

#include "global.h"

void	error_arg(char *name)
{
  my_printf("Usage: %s [file]\n", name);
  exit(EXIT_FAILURE);
}

void	error_malloc(char *str)
{
  my_printf("Memory access denied for var: %s\n", str);
  exit(EXIT_FAILURE);
}

void	error_file_path(char *name)
{
  my_printf("Error, the file %s is not accessible\n", name);
  exit(EXIT_FAILURE);
}

void	exit_msg(char *msg, int e)
{
  to_out(2, msg);
  exit(e);
}
