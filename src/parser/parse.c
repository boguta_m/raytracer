/*
** parce.c for rtv in /home/bousca_a/rendu/parser_rtv
**
** Made by Antonin Bouscarel
** Login   <bousca_a@epitech.net>
**
** Started on  Wed May 14 16:02:58 2014 Antonin Bouscarel
** Last update Sun Jun  8 21:14:45 2014 Maxime Boguta
*/

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "global.h"
#include "struct.h"

void	add_new_cam(t_all *all, char *str)
{
  char	**pos;
  char	**rpos;
  char	*tofree;

  pos = my_wordtab(tofree = find_content(str, "<pos>", "</pos>"), ':');
  free(tofree);
  rpos = my_wordtab(tofree = find_content(str, "<rvec>", "</rvec>"), ':');
  free(tofree);
  if (pos != NULL && rpos != NULL &&
      get_sizetab(pos) == 3 && get_sizetab(rpos) == 3)
    {
      all->ent.cam.pos[0] = my_getnbrf(pos[0]);
      all->ent.cam.pos[1] = my_getnbrf(pos[1]);
      all->ent.cam.pos[2] = my_getnbrf(pos[2]);
      all->ent.cam.rpos[0] = my_getnbrf(rpos[0]);
      all->ent.cam.rpos[1] = my_getnbrf(rpos[1]);
      all->ent.cam.rpos[2] = my_getnbrf(rpos[2]);
    }
  if (pos)
    my_free_wordtab(pos);
  if (rpos)
    my_free_wordtab(rpos);
}

void	add_cam(t_all *all, char *str)
{
  char	*tmp;

  all->ent.cam.pos[0] = 0;
  all->ent.cam.pos[1] = 0;
  all->ent.cam.pos[2] = 0;
  all->ent.cam.rpos[0] = 0;
  all->ent.cam.rpos[1] = 0;
  all->ent.cam.rpos[2] = 0;
  while (*str)
    {
      tmp = get_content(str, "<cam>", "</cam>");
      if (tmp)
	add_new_cam(all, str);
      free(tmp);
      ++str;
    }
}

void	add_name(t_all *all, char *str)
{
  char	*tmp;

  while (*str)
    {
      tmp = get_content(str, "<name>", "</name>");
      if (tmp)
	all->info.file = my_strdup(tmp);
      free(tmp);
      ++str;
    }
}

void	parse_file(t_all *all)
{
  int	fd;
  int	ret;
  char	*buff;
  int	i;

  if (!(all->cluster.content))
    {
      i = 1;
      buff = x_malloc(BUFF_SIZE + 1, "parser buffer");
      bzero(buff, BUFF_SIZE);
      if ((fd = open(all->info.file, O_RDONLY)) == -1)
	exit_msg("error : can't open file.\n", EXIT_FAILURE);
      while ((ret = read(fd, buff, BUFF_SIZE)) == BUFF_SIZE && ++i)
	{
	  buff = my_realloc(buff, (BUFF_SIZE * i) + 1, BUFF_SIZE * (i - 1));
	  buff[ret * i] = '\0';
	}
    }
  else
    buff = all->cluster.content;
  add_name(all, buff);
  add_obj(all, buff);
  add_light(all, buff);
  add_cam(all, buff);
  !all->cluster.content ? free(buff) : 0;
}
