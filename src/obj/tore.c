/*
** sphere.c for raytracer in /home/bousca_a/rendu/MUL_2013_raytracer/src/obj
**
** Made by Antonin Bouscarel
** Login   <bousca_a@epitech.net>
**
** Started on  Wed May 28 18:52:39 2014 Antonin Bouscarel
** Last update Sat Jun  7 17:19:12 2014 Maxime Boguta
*/

#include "struct.h"

void		_ntore(t_vector *n, double *i, t_obj *obj)
{
  double	tmp[6];
  t_vector	rot;

  tmp[0] = i[0] - obj->pos[0];
  tmp[1] = i[1] - obj->pos[1];
  tmp[2] = i[2] - obj->pos[2];
  rot.x = -obj->rpos[0];
  rot.y = -obj->rpos[1];
  rot.z = -obj->rpos[2];
  _rot_all_inv(NULL, &tmp, &rot);
  tmp[3] = CARRE(obj->size2);
  tmp[4] = CARRE(obj->size);
  tmp[5] = 4. * (CARRE(tmp[0]) + CARRE(tmp[1]) + CARRE(tmp[2])
		 + tmp[4] - tmp[3]);
  n->x = (tmp[0] * tmp[5]) - (8.000 * tmp[4] * tmp[0]);
  n->y = (tmp[1] * tmp[5]);
  n->z = (tmp[2] * tmp[5]) - (8.000 * tmp[4] * tmp[2]);
}

void		init_ind(t_tmp *tmp, t_obj *obj, double *indice)
{
  double	r;
  double	R;

  r = (double)tmp->cur->size2;
  R = (double)tmp->cur->size;
  indice[A] = CARRE(CARRE(VX) + CARRE(VY) + CARRE(VZ));
  indice[B] = 2.0 * (CARRE(VX) + CARRE(VY) + CARRE(VZ))
    * (2.0 * (VX * XEYE + VY * YEYE + VZ * ZEYE));
  indice[C] = 2.0 * (CARRE(VX) + CARRE(VY) + CARRE(VZ))
    * (CARRE(XEYE) + CARRE(YEYE) + CARRE(ZEYE) + CARRE(R)
       - CARRE(r)) + CARRE(2.0 *
			   (VX * XEYE + VY * YEYE + VZ * ZEYE)) - 4.0 * CARRE(R)
    * (CARRE(VX) + CARRE(VZ));
  indice[D] = 2.0 * (2.0 * (VX * XEYE + VY * YEYE + VZ * ZEYE))
    * (CARRE(XEYE) + CARRE(YEYE) + CARRE(ZEYE) + CARRE(R) - CARRE(r))
    - (4.0 * CARRE(R) * 2.0 * (VX * XEYE + VZ * ZEYE));
  indice[E] = CARRE(CARRE(XEYE) + CARRE(YEYE) + CARRE(ZEYE) + CARRE(R)
		    - CARRE(r)) - (4.0 * CARRE(R) * (CARRE(XEYE)
						   + CARRE(ZEYE)));
}

void		calc_tore(t_tmp *tmp, t_obj **obj)
{
  double	k;
  double	indice[5];

  init_ind(tmp, *obj, indice);
  k = equat4_smaller_k(indice);
  if (k < 0)
    return ;
  if ((*obj == NULL) || k < tmp->hit.dist)
    {
      *obj = tmp->cur;
      tmp->hit.dist = k;
    }
}
