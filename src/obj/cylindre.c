/*
** sphere.c for raytracer in /home/bousca_a/rendu/MUL_2013_raytracer/src/obj
**
** Made by Antonin Bouscarel
** Login   <bousca_a@epitech.net>
**
** Started on  Wed May 28 18:52:39 2014 Antonin Bouscarel
** Last update Sun Jun  8 14:47:46 2014 Maxime Boguta
*/

#include "struct.h"

void		_ncy(t_vector *n, double *i, t_obj *obj)
{
  t_vector	rot;

  n->x = i[0] - obj->pos[0];
  n->y = i[1] - obj->pos[1];
  n->z = i[2] - obj->pos[2];
  rot.x = -obj->rpos[0];
  rot.y = -obj->rpos[1];
  rot.z = -obj->rpos[2];
  _rot_all_inv(NULL, n, &rot);
  n->z = 0;
}

void		calc_cylindre(t_tmp *tmp, t_obj **obj)
{
  double	a[3];
  double	det;
  double	hit[2];

  a[0] = pow(VX, 2) + pow(VY, 2);
  a[1] = 2 * (XEYE * VX + YEYE * VY);
  a[2] = pow(XEYE, 2) + pow(YEYE, 2) - pow(tmp->cur->size, 2);
  det = pow(a[1], 2) - (4 * a[0] * a[2]);
  if (det < 0)
    return;
  hit[0] = (-a[1] - sqrtf(det)) / (2 * a[0]);
  hit[1] = (-a[1] + sqrtf(det)) / (2 * a[0]);
  if (hit[1] < hit[0] && hit[1] > 0)
    hit[0] = hit[1];
  if (hit[0] < 0)
    return;
  if ((*obj == NULL || hit[0] < tmp->hit.dist) && hit[0] > 0)
    {
      *obj = tmp->cur;
      tmp->hit.dist = hit[0];
    }
}
