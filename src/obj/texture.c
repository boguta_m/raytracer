/*
** texture.c for rt in /home/camill_n/mul_2013_raytracer
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sun Jun  8 02:52:13 2014 camill_n
** Last update Wed Jun 11 19:10:26 2014 Maxime Boguta
*/

#include "struct.h"

#define  FREQUENCE_PLAN 9
#define	 FREQUENCE 1

int	load_texture(t_all *all, char *path, t_img *img)
{
  int	ret;

  ret = 0;
  img->img_ptr = mlx_xpm_file_to_image(all->mlx.mlx_ptr,
				       path,
				       &(img->width),
				       &(img->heigh));
  ret = img->img_ptr == NULL ? 0 : 1;
  if (ret)
    {
      img->data = mlx_get_data_addr(img->img_ptr,
				    &img->bpp,
				    &img->size_line, &img->endian);
    }
  return (ret);
}

void		get_pos_text
(int *x, int *y, int *cur_px_calc, t_hit *hit, t_texture *image)
{
  int			texture[2];
  double		utils[3];
  double		rayon;

  if (hit->obj->type == PLAN)
    {
      texture[0] = (hit->sim[0] * FREQUENCE_PLAN +
		    image->img.width /2);
      texture[1] = (hit->sim[1] * FREQUENCE_PLAN +
		    image->img.heigh /2 );
      while (texture[0] < 0)
	texture[0] = texture[0] + (100 * image->img.width);
      while (texture[1] < 0)
	texture[1] = texture[1] + (100 * image->img.heigh);
      texture[0] = texture[0] % image->img.width;
      texture[1] = texture[1] % image->img.heigh;
    }
  if (hit->obj->type == SPHERE)
    {
      rayon = (double)hit->obj->size;
      utils[1] = atan(hit->sim[1] / hit->sim[0]);
      utils[2] = asin(hit->sim[2] / rayon);
      utils[2] += M_PI;
      utils[0] = (double)FREQUENCE * (utils[1] / M_PI + 0.5);
      texture[1] = fabs((int)(image->img.heigh * (utils[0] - (int)utils[0])));
      utils[0] = 0.5 * (double)FREQUENCE * utils[2] / M_PI;
      texture[0] = fabs((int)image->img.width * (utils[0] - (int)utils[0]));
      texture[0] = texture[0] % image->img.width;
      texture[1] = texture[1] % image->img.heigh;
    }
  *x = texture[0];
  *y = texture[1];
}


void		set_color_texture(t_all *all, t_hit *hit, int *cur_px_calc)
{
  int		x;
  int		y;
  int		z;
  int		pos;
  t_texture	*texture;
  t_obj		*obj;

  obj = hit->obj;
  texture = get_texture(all, obj->path_text);
  if (texture)
    {
      x = cur_px_calc[0] % texture->img.width;
      y = cur_px_calc[1] % texture->img.heigh;
      get_pos_text(&x, &y, cur_px_calc, hit, texture);
      pos = x * (texture->img.bpp / 8) + (y * texture->img.size_line);
      obj->color.r = (float)(unsigned char)texture->img.data[pos + 2];
      obj->color.g = (float)(unsigned char)texture->img.data[pos + 1];
      obj->color.b = (float)(unsigned char)texture->img.data[pos];
    }
  else
    obj->path_text = NULL;
}

void		add_texture(t_all *all, char *name, char *path)
{
  t_texture	*tmp;
  t_texture	*new_texture;

  new_texture = x_malloc(sizeof(t_texture), "new_texture");
  bzero(new_texture->name, 20);
  my_memcpy(new_texture->name, name, my_strlen(name));
  if (!(load_texture(all, path, &new_texture->img)))
    {
      free (new_texture);
      return ;
    }
  new_texture->next = NULL;
  if (all->ent.texture == NULL)
    all->ent.texture = new_texture;
  else
    {
      tmp = all->ent.texture;
      while (tmp->next != NULL)
	tmp = tmp->next;
      tmp->next = new_texture;
    }
}

void	init_texture(t_all *all)
{
  add_texture(all, "herb", "textures/sol.xpm");
  add_texture(all, "sky", "textures/sky.xpm");
  add_texture(all, "wall", "textures/wall.xpm");
  add_texture(all, "wood", "textures/wood.xpm");
  add_texture(all, "brick", "textures/brick.xpm");
  add_texture(all, "marble", "textures/marble.xpm");
  add_texture(all, "stone", "textures/stone.xpm");
}

t_texture	*get_texture(t_all *all, char *name)
{
  t_texture	*tmp;

  tmp = all->ent.texture;
  while (tmp)
    {
      if (!my_strcmp(tmp->name, name))
	return (tmp);
      tmp = tmp->next;
    }
  return (NULL);
}
