/*
** sphere.c for raytracer in /home/bousca_a/rendu/MUL_2013_raytracer/src/obj
**
** Made by Antonin Bouscarel
** Login   <bousca_a@epitech.net>
**
** Started on  Wed May 28 18:52:39 2014 Antonin Bouscarel
** Last update Sun Jun  8 14:52:50 2014 Maxime Boguta
*/

#include "struct.h"

void		_nplane(t_vector *n, double *i, t_obj *obj)
{
  t_vector	rot;

  n->x = 0.;
  n->y = 0.;
  n->z = 100.;
}

void		calc_plan(t_tmp *tmp, t_obj **obj)
{
  double	dis;

  if (fabs(VZ) > 0.000001)
    {
      if ((dis = -(ZEYE / VZ)) < 0)
	return ;
      if ((*obj == NULL || dis < tmp->hit.dist) && dis > 0)
	{
	  *obj = tmp->cur;
	  tmp->hit.dist = dis;
	}
    }
}
