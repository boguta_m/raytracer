/*
** scene.c for raytracer in /home/camill_n/rendu/mul_2013_raytracer
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Sun Jun  1 22:11:16 2014 camill_n
** Last update Wed Jun 11 16:17:44 2014 Maxime Boguta
*/

#include "struct.h"

void		call_obj_func(t_all *main, int type, t_tmp *tmp, t_obj **obj)
{
  t_all_obj	*func;
  int		check;

  check = 1;
  func = main->all_obj;
  while (check && func)
    {
      if (func->type == type)
	{
	  func->exec(tmp, obj);
	  --check;
	}
      func = func->next;
    }
}

void		launch_calc(t_all *all, int px, int *pos, int nbt)
{
  int		size_img;
  t_color	color;
  int		i;

  i = 0;
  size_img = all->thread_interval;
  while (i < size_img)
    {
      pos[0] = px % WIDTH;
      pos[1] = px / WIDTH;
      get_pix(all, pos[0], pos[1], &color);
      put_pixel(&all->img, pos[0], pos[1], &color);
      ++pos[0];
      px += all->nb_thread;
      ++all->info.cpt[nbt];
      ++i;
    }
}

void		*calc_scene(void *ptr_all)
{
  int		px;
  int		pos[2];
  int		nb_thread;
  t_all		*all;

  all = ptr_all;
  px = all->thread_s;
  nb_thread = all->thread_free - 1;
  all->thread_s = -1;
  my_memset(pos, 8);
  launch_calc(all, px, pos, nb_thread);
  return (NULL);
}
