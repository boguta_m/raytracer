/*
** init.c for mysh in /home/camill_n/rendu/PSU_2013_minishell2
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Mon Feb 10 20:50:47 2014 camill_n
** Last update Sun Jun  8 22:50:31 2014 Maxime Boguta
*/

#include "global.h"
#include "struct.h"

void		usage()
{
  to_out(2, "Usage : ./rt <scene file> ");
  to_out(2, "[-t [THREAD_AMT]] [-o [OUTPUT]] [-qD -nS -nB -nE]\n");
  exit(EXIT_FAILURE);
}

void		init_win(t_all *all)
{
  char		*name;

  name = x_malloc(my_strlen(all->info.file) + 30, "win name");
  my_strcpy(name, "Raytracer - Loaded scene : ");
  my_strcatt(name, all->info.file);
  MLX.win_ptr = mlx_new_window(MLX.mlx_ptr, WIDTH, HEIGHT, name);
  free(name);
}

void		init_img(t_all *all)
{
  t_light	*lig;

  lig = all->ent.light;
  if ((MLX.mlx_ptr = mlx_init()) == NULL)
    exit_msg("Error : Can't set display to current display environnement.\n");
  IMG.img_ptr = mlx_new_image(MLX.mlx_ptr, WIDTH, HEIGHT);
  IMG.data = mlx_get_data_addr(IMG.img_ptr, &IMG.bpp,
			       &IMG.size_line, &IMG.endian);
  all->info.nb_lights = 0;
  while (lig != NULL)
    {
      all->info.nb_lights++;
      lig = lig->next;
    }
}

void		init_func(t_all *all)
{
  add_obj_in_list(all, SPHERE, calc_sphere);
  add_obj_in_list(all, PLAN, calc_plan);
  add_obj_in_list(all, CYLINDRE, calc_cylindre);
  add_obj_in_list(all, CONE, calc_cone);
  add_obj_in_list(all, TORE, calc_tore);
  add_obj_in_list(all, CUBE_T, calc_cubet);
}

void		init_all(t_all *all)
{
  all->all_obj = NULL;
  all->ent.texture = NULL;
  init_img(all);
  init_func(all);
  init_texture(all);
}
