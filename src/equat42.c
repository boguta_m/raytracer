/*
** equat42.c for rt in /home/boguta_m/rendu/mul_2013_raytracer
**
** Made by Maxime Boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Thu Jun  5 00:34:57 2014 Maxime Boguta
** Last update Thu Jun  5 00:36:29 2014 Maxime Boguta
*/

#include "struct.h"
#include "equat4.h"

double		calcul_solution_smallest(double *solution)
{
  if (solution[0] >= 0.0 && (solution[0] < solution[1]) && \
      (solution[0] < solution[2]) && (solution[0] < solution[3]))
    return (solution[0]);
  else if (solution[1] >= 0.0 && (solution[1] < solution[0]) && \
      (solution[1] < solution[2]) && (solution[1] < solution[3]))
    return (solution[1]);
  else if (solution[2] >= 0.0 && (solution[2] < solution[0]) && \
      (solution[2] < solution[1]) && (solution[2] < solution[3]))
    return (solution[2]);
  else if (solution[3] >= 0.0 && (solution[3] < solution[0]) && \
      (solution[3] < solution[1]) && (solution[3] < solution[2]))
    return (solution[3]);
  return (-0.001);
}
