/*
** flags2.c for rt in /home/boguta_m/rendu/mul_2013_raytracer
**
** Made by Maxime Boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Fri Jun  6 15:54:14 2014 Maxime Boguta
** Last update Sun Jun  8 22:58:30 2014 Maxime Boguta
*/

#include "struct.h"
#include "flags.h"
#include "network.h"

inline void	first_check(t_all *all, int op, char **av, int i)
{
  if ((all->info.flags & op) != 0)
    {
      if (op & NO_SHADOWS || op & NO_EFFECTS || op & NO_BRIGHT)
	to_out(1, "info : -qD and all drawing options are exclusives.\n");
      exit_msg("error : option enabled twice.\n", EXIT_FAILURE);
    }
  all->info.flags += op;
}

inline int	thread_check(t_all *all, int op, char **av, int i)
{
  if (op & THREADING_OPT)
    {
      if (((av[i + 1][0] != 0 && av[i + 1][0] != '-')) &&
	  (all->nb_thread = my_getnbr(av[i + 1])) <= 0 || all->nb_thread > 256)
	{
	  if (all->nb_thread > 256)
	    to_out(1, "info : more than 8 threads can harm performance.\n");
	  exit_msg("error : thread amount is unhandable\n."
		   , EXIT_FAILURE);
	}
      else if (av[i + 1][0] == 0 || av[i + 1][0] == '-')
	return (0);
      return (1);
    }
  return (-1);
}

inline int	aa_check(t_all *all, int op, char **av, int i)
{
  if (op & AA_OPT)
    {
      if (((av[i + 1][0] != 0 && av[i + 1][0] != '-')) &&
	  (all->info.aa = my_getnbr(av[i + 1])) < 1 && all->info.aa > 32)
	{
	  exit_msg("error : AA amount is unhandable\n."
		   , EXIT_FAILURE);
	}
      else if (av[i + 1][0] == 0 || av[i + 1][0] == '-')
	return (0);
      return (1);
    }
  return (-1);
}

inline int	cluster_check(t_all *all, int op, char **av, int i)
{
  if (op & CLIENT)
    {
      if (av[i + 1][0] == 0 || av[i + 1][0] == '-')
	exit_msg("error : can't set host.\n"
		 , EXIT_FAILURE);
      return (1);
    }
  return (-1);
}

int		is_existant(t_all *all, int op, char **av, int i)
{
  int		ret;

  first_check(all, op, av, i);
  if ((op & THREADING_OPT || op & GEN_OUTPUT || op & AA_OPT || op & CLIENT)
      && av[i + 1])
    {
      if ((ret = thread_check(all, op, av, i)) != -1  ||
	  (ret = aa_check(all, op, av, i) != -1) ||
	  (ret = cluster_check(all, op, av, i) != -1))
	return (ret);
      if (op & GEN_OUTPUT)
	{
	  if ((OUTPATH = my_realloc(OUTPATH, my_strlen(av[i + 1]), 11)) != NULL
	      && av[i + 1][0] != 0 && av[i + 1][0] != '-')
	    my_strcpy(OUTPATH, av[i + 1]);
	  else
	    return (0);
	  return (1);
	}
      else if ((op & THREADING_OPT || op & GEN_OUTPUT) && !av[i + 1])
	exit_msg("error : '-t' or '-o' : argument missing.\n", EXIT_FAILURE);
    }
  if (!(op & GEN_OUTPUT) && (op & THREADING_OPT || op & AA_OPT || op & CLIENT))
    exit_msg("error : option needs argument.\n", EXIT_FAILURE);
  return (0);
}
