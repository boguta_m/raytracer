/*
** colors.c for rtv1 in /home/boguta_m/rendu/MUL_2013_rtv1
**
** Made by Maxime Boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Fri Feb 28 13:19:28 2014 Maxime Boguta
** Last update Sun Jun  8 20:25:15 2014 Maxime Boguta
*/

#include "struct.h"
#include "colors.h"

void		_mix_colors(t_color *from, t_color *to, int stren, t_tmp *tmp)
{
  int		difr;
  int		difg;
  int		difb;
  double	fac;
  double	faclight;
  double	shiny;

  faclight = tmp->sca;
  fac = (double)stren / 100;
  shiny = tmp->hit.obj->shine / 100;
  if (faclight < 0)
    return ;
  to->r += (from->r * fac * faclight) * shiny;
  to->g += (from->g * fac * faclight) * shiny;
  to->b += (from->b * fac * faclight) * shiny;
  to->r > 255 ? to->r = 255 : to->r < 0 ? to->r = 0 : 0;
  to->g > 255 ? to->g = 255 : to->g < 0 ? to->g = 0 : 0;
  to->b > 255 ? to->b = 255 : to->b < 0 ? to->b = 0 : 0;
}

void		_checks_color(t_color *col, t_tmp *tmp, t_light *lig)
{
  if (lig->type == 1)
    {
      col->r > 255 ? col->r = 255 : col->r < 0 ? col->r = 0 : 0;
      col->g > 255 ? col->g = 255 : col->g < 0 ? col->g = 0 : 0;
      col->b > 255 ? col->b = 255 : col->b < 0 ? col->b = 0 : 0;
    }
  else
    {
      col->r > tmp->hit.obj->color.r ? col->r = tmp->hit.obj->color.r :
	col->r < 0 ? col->r = 0 : 0;
      col->g > tmp->hit.obj->color.g ? col->g = tmp->hit.obj->color.g :
	col->g < 0 ? col->g = 0 : 0;
      col->b > tmp->hit.obj->color.b ? col->b = tmp->hit.obj->color.b :
	col->b < 0 ? col->b = 0 : 0;
    }
}
void		_color_factor(t_tmp *tmp, t_color *col, t_light *lig, double fac)
{
  double	sfac;

  if ((sfac = fac * ((double)lig->strengh / 100)) < 0)
    return ;
  col->r += (sfac * tmp->hit.obj->color.r);
  col->g += (sfac * tmp->hit.obj->color.g);
  col->b += (sfac * tmp->hit.obj->color.b);
  _checks_color(col, tmp, lig);
}

void		_simple_color(t_tmp *tmp, t_color *col, double fac)
{
  col->r += (tmp->hit.obj->color.r * fac);
  col->g += (tmp->hit.obj->color.g * fac);
  col->b += (tmp->hit.obj->color.b * fac);
  col->r > 255 ? col->r = 255 : col->r < 0 ? col->r = 0 : 0;
  col->g > 255 ? col->g = 255 : col->g < 0 ? col->g = 0 : 0;
  col->b > 255 ? col->b = 255 : col->b < 0 ? col->b = 0 : 0;
}
