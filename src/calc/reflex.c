/*
** reflex.c for rt in /home/boguta_m/rendu/mul_2013_raytracer
**
** Made by Maxime Boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Sat Jun  7 00:51:43 2014 Maxime Boguta
** Last update Sun Jun  8 23:00:40 2014 Maxime Boguta
*/

#include "struct.h"
#include "flags.h"

void		_init_pov(t_all *main, t_tmp *tmp)
{
  tmp->hit.hit[0] = main->ent.cam.pos[0];
  tmp->hit.hit[1] = main->ent.cam.pos[1];
  tmp->hit.hit[2] = main->ent.cam.pos[2];
}

void		_replace_pov(t_tmp *tmp)
{
  t_vector	nnorm;
  t_vector	nsav;
  double	dotp;

  get_norm(&tmp->norm, &tmp->hit.hit[0], tmp->hit.obj);
  dotp = dot_product(&tmp->realvec, &tmp->norm);
  tmp->realvec.x = (tmp->realvec.x - 2. * dotp
		    * tmp->norm.x);
  tmp->realvec.y = (tmp->realvec.y - 2. * dotp
		    * tmp->norm.y);
  tmp->realvec.z = (tmp->realvec.z - 2. * dotp
		    * tmp->norm.z);
  tmp->hit.hit[0] += (tmp->realvec.x / 100);
  tmp->hit.hit[1] += (tmp->realvec.y / 100);
  tmp->hit.hit[2] += (tmp->realvec.z / 100);
}

int		is_reflex(t_tmp *tmp, t_all *main)
{
  if (tmp->hit.fhit && tmp->hit.obj)
    if (tmp->hit.obj->reflex > 0 && tmp->ray.bounce < MAX_BOUNCE
	&& (!(main->info.flags & NO_EFFECTS)))
      return (1);
  return (0);
}

void		_mix_reflex(t_color *to, t_color *fr, double fac)
{
  to->r = (fr->r * fac) + to->r * (1 - fac);
  to->g = (fr->g * fac) + to->g * (1 - fac);
  to->b = (fr->b * fac) + to->b * (1 - fac);
  to->r > 255 ?
    to->r = 255 : to->r < 0 ? to->r = 0 : 0;
  to->g > 255 ?
    to->g = 255 : to->g < 0 ? to->g = 0 : 0;
  to->b > 255 ?
    to->b = 255 : to->b < 0 ? to->b = 0 : 0;
}

void		_reflex(t_all *main, t_tmp *tmp)
{
  t_color	sav;
  t_color	sav2;
  double	fac;
  double	ffac;

  my_memcpy(&sav, &tmp->color, sizeof(t_color));
  ffac = (double)tmp->hit.fhit->reflex / 100;
  while (is_reflex(tmp, main))
    {
      my_memset(&sav2, sizeof(t_color));
      if (tmp->hit.obj)
	_replace_pov(tmp);
      _checks(main, tmp);
      if (tmp->hit.obj)
  	_factors(main, &sav2, tmp);
      _mix_reflex(&sav, &sav2, ffac);
      ++tmp->ray.bounce;
    }
  my_memcpy(&tmp->color, &sav, sizeof(t_color));
}
