/*
** checks.c for rt in /home/boguta_m/rendu/mul_2013_raytracer
**
** Made by Maxime Boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Sat Jun  7 00:54:01 2014 Maxime Boguta
** Last update Wed Jun 11 18:13:19 2014 Maxime Boguta
*/

#include "struct.h"

void		_match(t_all *main, t_tmp *tmp, t_obj **obj)
{
  call_obj_func(main, tmp->cur->type, tmp, obj);
}

void		_move(t_all *main, t_tmp *tmp)
{
  t_vector	rot;

  tmp->ray.start.x = tmp->hit.hit[0] - tmp->cur->pos[0];
  tmp->ray.start.y = tmp->hit.hit[1] - tmp->cur->pos[1];
  tmp->ray.start.z = tmp->hit.hit[2] - tmp->cur->pos[2];
  rot.x = -tmp->cur->rpos[0];
  rot.y = -tmp->cur->rpos[1];
  rot.z = -tmp->cur->rpos[2];
  _rot_all_inv(main, &tmp->ray.start, &rot);
  tmp->ray.dir.x = tmp->realvec.x;
  tmp->ray.dir.y = tmp->realvec.y;
  tmp->ray.dir.z = tmp->realvec.z;
  _rot_all_inv(main, &tmp->ray.dir, &rot);
}

void		_checks(t_all *main, t_tmp *tmp)
{
  tmp->hit.dist = 10000;
  tmp->cur = main->ent.obj;
  tmp->hit.obj = NULL;
  while (tmp->cur != NULL)
    {
      _move(main, tmp);
      _match(main, tmp, &tmp->hit.obj);
      tmp->cur = tmp->cur->next;
    }
  if (tmp->hit.obj)
    {
      tmp->cur = tmp->hit.obj;
      _move(main, tmp);
      _hit_cd(main, tmp);
    }
}
