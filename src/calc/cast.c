/*
** cast for rt in /home/boguta_m/rendu/mul_2013_raytracer/src/calc
**
** Made by Maxime Boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Fri Jun  6 01:07:24 2014 Maxime Boguta
** Last update Sun Jun  8 20:10:27 2014 Maxime Boguta
*/

#include <time.h>
#include "flags.h"
#include "struct.h"

inline int	_init_soft(int *hray, t_light *lig)
{
  *hray = 0;
  return (lig->soft);
}

unsigned int	_case_lightbox
(unsigned int cast, t_light *lig, t_tmp *tmp, t_all *main)
{
  t_light	new;
  double	alea;
  unsigned int	hray;

  hray = 0;
  my_memcpy(&new, lig, sizeof(t_light));
  while (cast)
    {
      alea = drand48() * lig->size;
      new.pos[0] = alea + lig->pos[0] - (lig->size / 2);
      alea = drand48() * lig->size;
      new.pos[1] = alea + lig->pos[1] - (lig->size / 2);
      alea = drand48() * lig->size;
      new.pos[2] = alea + lig->pos[2] - (lig->size / 2);
      _get_l(&tmp->hit, &new, &tmp->lig);
      get_norm(&tmp->norm, &tmp->hit.hit[0], tmp->hit.obj);
      if (_shadows(main, tmp, &new))
	{
	  ++hray;
	  get_lig_inf(tmp, &new);
	}
      --cast;
    }
  return (hray);
}

double		soft_light(t_light *lig, t_tmp *tmp, t_all *main)
{
  unsigned int	hray;
  unsigned int	cast;
  double	real_pos[3];

  cast = _init_soft(&hray, lig);
  if (main->info.flags & NO_SHADOWS)
    return (1.);
  if (!(main->info.flags & SIMPLE_LIGHTS) && lig->shape)
    {
      if (lig->shape == 1 && lig->soft > 1)
	hray = _case_lightbox(cast, lig, tmp, main);
    }
  else if ((main->info.flags & SIMPLE_LIGHTS)
	   || lig->shape == 0 || lig->soft < 2)
    return ((double)_shadows(main, tmp, lig));
  return ((double)hray / lig->soft);
}
