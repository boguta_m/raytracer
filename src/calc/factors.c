/*
** factors.c for rtv1 in /home/boguta_m/rendu/MUL_2013_rtv1
**
** Made by Maxime Boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Thu Feb 27 16:08:05 2014 Maxime Boguta
** Last update Wed Jun 11 18:13:14 2014 Maxime Boguta
*/

#include "calc.h"
#include "flags.h"
#include "struct.h"
#include "colors.h"

void		_hit_cd(t_all *main, t_tmp *tmp)
{
  tmp->hit.hit[0] = tmp->hit.hit[0]
    + (tmp->hit.dist * tmp->realvec.x);
  tmp->hit.hit[1] = tmp->hit.hit[1]
    + (tmp->hit.dist * tmp->realvec.y);
  tmp->hit.hit[2] = tmp->hit.hit[2]
    + (tmp->hit.dist * tmp->realvec.z);
  tmp->hit.sim[0] = tmp->ray.start.x
    + (tmp->hit.dist * tmp->ray.dir.x);
  tmp->hit.sim[1] = tmp->ray.start.y
    + (tmp->hit.dist * tmp->ray.dir.y);
  tmp->hit.sim[2] = tmp->ray.start.z
    + (tmp->hit.dist * tmp->ray.dir.z);
}

void		_shiness(t_light *lig, t_color *color, t_tmp *tmp)
{
  tmp->hit.obj->shine > 100 ? tmp->hit.obj->shine = 100 :
    tmp->hit.obj->shine < 0 ? tmp->hit.obj->shine = 0 : 0;
  _mix_colors(&lig->color, color, lig->strengh, tmp);
}

void		_lights(t_all *main, t_light *lig, t_color *col, t_tmp *tmp)
{
  double	ret;
  double	fac;

  tmp->sca = -1;
  _get_l(&tmp->hit, lig, &tmp->lig);
  get_norm(&tmp->norm, &tmp->hit.hit[0], tmp->hit.obj);
  get_lig_inf(tmp, lig);
  ret = soft_light(lig, tmp, main);
  if (tmp->hit.obj && tmp->hit.obj->path_text)
    set_color_texture(main, &tmp->hit, tmp->cur_px_calc);
  _shiness(lig, col, tmp);
  if (!(main->info.flags & NO_BRIGHT))
    _color_factor(tmp, col, lig, tmp->sca * ret);
  else
    _simple_color(tmp, col, 1 * ret);
}

void		_factors(t_all *main, t_color *col, t_tmp *tmp)
{
  t_light	*lig;
  unsigned int	soft;

  lig = main->ent.light;
  while (lig != NULL)
    {
      _lights(main, lig, col, tmp);
      lig = lig->next;
    }
}
