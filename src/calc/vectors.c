/*
** vectors.c for rtv1 in /home/boguta_m/rendu/MUL_2013_rtv1
**
** Made by Maxime Boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Tue Feb 25 14:48:37 2014 Maxime Boguta
** Last update Sun Jun  8 20:16:23 2014 Maxime Boguta
*/

#include "struct.h"

double		_normalize(t_vector *v)
{
  double	ret;

  ret = sqrt(CARRE(v->x) + CARRE(v->y) + CARRE(v->z));
  return (ret);
}

void		_unitize(t_vector *v)
{
  double	size;

  size = _normalize(v);
  v->x /= size;
  v->y /= size;
  v->z /= size;
}

void		_get_l(t_hit *hit, t_light *lig, t_vector *bri)
{
  bri->x = lig->pos[0] - hit->hit[0];
  bri->y = lig->pos[1] - hit->hit[1];
  bri->z = lig->pos[2] - hit->hit[2];
}

double		dot_product(t_vector *a, t_vector *b)
{
  return	(a->x * b->x + a->y * b->y + a->z * b->z);
}

void		get_lig_inf(t_tmp *tmp, t_light *lig)
{
  double	sca;
  double	dot;
  double	na;
  double	nb;
  double	sa;

  sca = dot_product(&tmp->norm, &tmp->lig);
  dot = _normalize(&tmp->norm) * _normalize(&tmp->lig);
  sa = sca / dot;
  tmp->sca = sa;
}
