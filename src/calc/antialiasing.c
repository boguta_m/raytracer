/*
** antialiasing.c for rt in /home/boguta_m/rendu/bk/mul_2013_raytracer
**
** Made by Maxime Boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Sat Jun  7 21:06:18 2014 Maxime Boguta
** Last update Sun Jun  8 22:55:02 2014 Maxime Boguta
*/

#include <time.h>
#include "struct.h"

void		_assemble_colors(t_color *to, t_color *from, int fac)
{
  to->r += (from->r / (double)fac);
  to->g += (from->g / (double)fac);
  to->b += (from->b / (double)fac);
}

inline void	init_aa(int *cellct, int *pass, int *subcell, t_all *main)
{
  *cellct = -1;
  *pass = 0;
  *subcell = CARRE(main->info.aa);
}

void		aa_handling(t_all *main, int x, int y, t_color *col)
{
  t_tmp		tmp;
  double	genx;
  double	geny;
  int		subcell;
  int		pass;
  int		cellct;
  double	rand;

  init_aa(&cellct, &pass, &subcell, main);
  while (++cellct < subcell)
    {
      cellct % main->info.aa == 0 ? pass++ : 0;
      rand = (0.5 - drand48()) / subcell;
      genx = (double)x - 0.5 + ((cellct % main->info.aa)
				* (1 / (double)main->info.aa)) + rand;
      rand = (0.5 - drand48()) / subcell;
      geny = (double)y - 0.5 + (pass * (1 / (double)main->info.aa)) + rand;
      my_memset(&tmp, sizeof(t_tmp));
      tmp.cur_px_calc[0] = x;
      tmp.cur_px_calc[1] = y;
      subcell > 1 ? _eye(main, genx, geny, &tmp)
	: _eye(main, (double)x, (double)y, &tmp);
      _trace(main, col, &tmp);
      _assemble_colors(col, &tmp.color, subcell);
    }
}
