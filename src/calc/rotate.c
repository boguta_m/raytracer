/*
** rotate.c for rtv1 in /home/boguta_m/rendu/MUL_2013_rtv1
**
** Made by Maxime Boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Thu Feb 27 19:10:16 2014 Maxime Boguta
** Last update Thu Jun  5 00:20:38 2014 Maxime Boguta
*/

#include "struct.h"

void		_rotx(t_vector *pos, t_vector *rot)
{
  double	mat[9];
  double	posx;
  double	posy;
  double	posz;

  mat[0] = 1;
  mat[1] = 0;
  mat[2] = 0;
  mat[3] = 0;
  mat[4] = cos(RAD_MAGIC(rot->x));
  mat[5] = -sin(RAD_MAGIC(rot->x));
  mat[6] = 0;
  mat[7] = sin(RAD_MAGIC(rot->x));
  mat[8] = cos(RAD_MAGIC(rot->x));
  posx = mat[0] * pos->x + mat[1] * pos->y + mat[2] * pos->z;
  posy = mat[3] * pos->x + mat[4] * pos->y + mat[5] * pos->z;
  posz = mat[6] * pos->x + mat[7] * pos->y + mat[8] * pos->z;
  pos->x = posx;
  pos->y = posy;
  pos->z = posz;
}

void		_roty(t_vector *pos, t_vector *rot)
{
  double	mat[9];
  double	posx;
  double	posy;
  double	posz;

  mat[0] = cos(RAD_MAGIC(rot->y));
  mat[1] = 0;
  mat[2] = sin(RAD_MAGIC(rot->y));
  mat[3] = 0;
  mat[4] = 1;
  mat[5] = 0;
  mat[6] = -sin(RAD_MAGIC(rot->y));
  mat[7] = 0;
  mat[8] = cos(RAD_MAGIC(rot->y));
  posx = mat[0] * pos->x + mat[1] * pos->y + mat[2] * pos->z;
  posy = mat[3] * pos->x + mat[4] * pos->y + mat[5] * pos->z;
  posz = mat[6] * pos->x + mat[7] * pos->y + mat[8] * pos->z;
  pos->x = posx;
  pos->y = posy;
  pos->z = posz;
}

void		_rotz(t_vector *pos, t_vector *rot)
{
  double	mat[9];
  double	posx;
  double	posy;
  double	posz;

  mat[0] = cos(RAD_MAGIC(rot->z));
  mat[1] = -sin(RAD_MAGIC(rot->z));
  mat[2] = 0;
  mat[3] = sin(RAD_MAGIC(rot->z));
  mat[4] = cos(RAD_MAGIC(rot->z));
  mat[5] = 0;
  mat[6] = 0;
  mat[7] = 0;
  mat[8] = 1;
  posx = mat[0] * pos->x + mat[1] * pos->y + mat[2] * pos->z;
  posy = mat[3] * pos->x + mat[4] * pos->y + mat[5] * pos->z;
  posz = mat[6] * pos->x + mat[7] * pos->y + mat[8] * pos->z;
  pos->x = posx;
  pos->y = posy;
  pos->z = posz;
}

void	_rot_all(t_all *main, t_vector *pos, t_vector *rot)
{
  _rotx(pos, rot);
  _roty(pos, rot);
  _rotz(pos, rot);
}

void	_rot_all_inv(t_all *main, t_vector *pos, t_vector *rot)
{
  _rotz(pos, rot);
  _roty(pos, rot);
  _rotx(pos, rot);
}
