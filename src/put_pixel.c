/*
** my_pixel_put_in_img.c for my_pixel_put_in_img in /home/boguta_m/rendu/mylibX
**
** Made by maxime boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Tue Dec  3 11:31:47 2013 maxime boguta
** Last update Thu May 29 17:54:10 2014 Maxime Boguta
*/

#include "struct.h"

void		put_pixel(t_img *mlx, int x, int y, t_color *color)
{
  int		pos;

  pos = (mlx->size_line * y + (x * (mlx->bpp / 8)));
  mlx->data[pos] = color->b;
  mlx->data[pos + 1] = color->g;
  mlx->data[pos + 2] = color->r;
  mlx->data[pos + 3] = color->alpha;
}
