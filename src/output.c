/*
** output.c for bmp in /home/boguta_m/rendu/mul_2013_raytracer/src
**
** Made by Maxime Boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Sat May 31 23:03:39 2014 Maxime Boguta
** Last update Fri Jun  6 04:32:15 2014 Maxime Boguta
*/

#include <stdio.h>
#include "flags.h"
#include "struct.h"

void		fill_headers
(unsigned char *fheader, unsigned char *iheader, unsigned int fsize)
{
  fheader[0] = 'B';
  fheader[1] = 'M';
  fheader[2] = (unsigned char)(fsize);
  fheader[3] = (unsigned char)(fsize >> 8);
  fheader[4] = (unsigned char)(fsize >> 16);
  fheader[5] = (unsigned char)(fsize >> 24);
  iheader[4] = (unsigned char)(WIDTH);
  iheader[5] = (unsigned char)(WIDTH >> 8);
  iheader[6] = (unsigned char)(WIDTH >> 16);
  iheader[7] = (unsigned char)(WIDTH >> 24);
  iheader[8] = (unsigned char)(HEIGHT);
  iheader[9] = (unsigned char)(HEIGHT >> 8);
  iheader[10] = (unsigned char)(HEIGHT >> 16);
  iheader[11] = (unsigned char)(HEIGHT >> 24);
}

FILE		*write_bmp_hea
(unsigned char *fheader, unsigned char *iheader, t_all *all)
{
  FILE		*fd;
  char		*out;

  out = x_malloc(my_strlen(all->info.outimg) + 5, "output bmp file");
  if (all->info.outimg == NULL || all->info.outimg[0] == 0)
    {
      to_out(2, "error : error while creating output file.\n");
      return (NULL);
    }
  my_strcpy(out, all->info.outimg);
  my_strcatt(out, ".bmp");
  free(all->info.outimg);
  if ((fd = fopen(out, "w+")) == NULL)
    return (fd);
  fwrite(fheader, 1, 14, fd);
  fwrite(iheader, 1, 40, fd);
  free(out);
  return (fd);
}

void		write_bmp_data(t_all *all, unsigned char *pad, FILE *fd)
{
  int		j;

  j = HEIGHT;
  while (--j > -1)
    {
      fwrite((all->img.data + (WIDTH * j * 4)), 4, WIDTH, fd);
      fwrite(pad, 1, ((4 - (HEIGHT * 4) % 4) % 4), fd);
    }
  fclose(fd);
}

void		save_as_bmp(t_all *all)
{
  unsigned int	fsize;
  unsigned char pad[3];
  unsigned char *fheader;
  unsigned char *iheader;
  FILE		*fd;

  pad[0] = 0;
  pad[1] = 0;
  pad[2] = 0;
  if ((all->info.flags & GEN_OUTPUT) == 0)
    return ;
  fheader = x_malloc(14, "header");
  iheader = x_malloc(40, "header");
  my_memset(fheader, 14);
  my_memset(iheader, 40);
  fsize = 54 + (3 * (HEIGHT * WIDTH));
  fill_headers(fheader, iheader, fsize);
  fheader[10] = 54;
  iheader[0] = 40;
  iheader[12] = 1;
  iheader[14] = 32;
  if ((fd = write_bmp_hea(fheader, iheader, all)) != NULL)
    write_bmp_data(all, pad, fd);
  free(fheader);
  free(iheader);
}
