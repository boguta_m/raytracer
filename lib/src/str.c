/*
** str.c for str in /home/camill_n/rendu/PSU_2013_minishell2
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Tue Feb 11 14:41:54 2014 camill_n
** Last update Sat Jun  7 17:12:47 2014 Maxime Boguta
*/

#include "global.h"

int	my_strncmpp(char *s1, char *s2, int i)
{
  int	j;

  j = -1;
  if (!s1 || !s2)
    return (-4);
  if (i == 0)
    return (1);
  while (++j < i && s1[j] != '\0' && s2[j] != '\0')
    {
      if (s1[j] != s2[j])
        return (s1[j] - s2[j]);
    }
  if ((s1[j] == '\0' && s2[j] == '\0') || i == j)
    return (0);
  if (s1[j] == '\0')
    return (0 - s2[j]);
  else
    return (s1[j]);
}

void	my_strcpy(char *s, char *d)
{
  int	i;

  i = -1;
  if (s && d)
    {
      while (d[++i])
	s[i] = d[i];
    }
  s[i] = 0;
}

char	*my_strdup(char *str)
{
  char	*tmp;
  int	i;

  tmp = x_malloc((my_strlen(str) + 1) * sizeof(char), "tmp");
  i = 0;
  while (str[i] != '\0')
    {
      tmp[i] = str[i];
      ++i;
    }
  tmp[i] = '\0';
  return (tmp);
}

void	to_out(int fd, char *m)
{
  if (write(fd, m, my_strlen(m)) == -1)
    exit(EXIT_FAILURE);
}

int	my_strcmp(char *s1, char *s2)
{
  int	i;

  i = 0;
  if (!s1 || !s2)
    return (-4);
  while (s1 && s2 && s1[i] && s2[i] && s1[i] == s2[i])
    i++;
  if (s1[i] == s2[i])
    return (0);
  return (-1);
}
