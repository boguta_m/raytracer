/*
** my_realloc.c for my_realloc in /home/boguta_m/rendu/mylibC/srcs
**
** Made by Maxime Boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Mon Jan  6 15:52:17 2014 Maxime Boguta
** Last update Mon Jan  6 16:08:23 2014 Maxime Boguta
*/

# include <unistd.h>
# include <stdlib.h>

void		*my_realloc(void *ptr, size_t new_size, size_t old_size)
{
  char		*ptr2;
  char		*tmp;
  unsigned int	i;

  i = -1;
  if (new_size == 0)
    {
      free(ptr);
      return (ptr);
    }
    if (ptr == NULL)
      return (malloc(new_size));
    if (old_size <= new_size)
      {
	if ((ptr2 = malloc(new_size)) == NULL)
	  return (NULL);
	tmp = ptr;
	while (++i <= old_size)
	  ptr2[i] = tmp[i];
	free(ptr);
	return (ptr2);
      }
    else
      return (ptr);
}
