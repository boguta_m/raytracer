/*
** my_evil_str.c for my_evil_str in /home/camill_n/rendu/Piscine-C-Jour_04
**
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
**
** Started on  Thu Oct  3 12:07:16 2013 Nicolas Camilli
** Last update Tue Feb 11 00:12:17 2014 camill_n
*/

#include "global.h"

char	*my_revstr(char *str)
{
  int	i;
  int	size;
  char	tmp;
  int	j;

  i = 0;
  size = my_strlen(str);
  j = size - 1;
  while (i < size / 2)
    {
      tmp = str[i];
      str[i] = str[j];
      str[j] = tmp;
      i = i + 1;
      j = j - 1;
    }
  return (str);
}
