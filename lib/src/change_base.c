/*
** change_base.c for my_printf in /home/camill_n/rendu/PSU_2018_my_printf
**
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
**
** Started on  Thu Nov 14 17:28:47 2013 Nicolas Camilli
** Last update Fri Nov 15 14:25:05 2013 Nicolas Camilli
*/

#include "../includes/func.h"

void	manage_put_octal(va_list ap)
{
  my_put_octal(va_arg(ap, unsigned int));
}

void	manage_put_unsigned(va_list ap)
{
  my_put_unsigned(va_arg(ap, unsigned int));
}

void	my_put_octal(unsigned int nb)
{
  int	tmp;

  if (nb > 0)
    {
      tmp = nb % 8;
      nb = nb / 8;
      my_put_octal(nb);
      my_putchar(tmp + 48);
    }
}

void	my_put_unsigned(unsigned int nb)
{
  int	tmp;

  if (nb > 0)
    {
      tmp = nb % 10;
      nb = nb / 10;
      my_put_unsigned(nb);
      my_putchar(tmp + 48);
    }
}
