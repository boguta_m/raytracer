/*
** mem.c for mem in /home/boguta_m/rendu/mul_2013_raytracer/lib/src
**
** Made by Maxime Boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Thu May 29 18:32:28 2014 Maxime Boguta
** Last update Sun Jun  1 00:47:05 2014 Maxime Boguta
*/

void	my_memcpy(void *pdest, void *psrc, int s)
{
  char	*cd;
  char	*cs;

  cs = psrc;
  cd = pdest;
  while (--s > -1)
    cd[s] = cs[s];
}

void	my_memset(void *p, int s)
{
  int	i;
  char	*pd;

  pd = p;
  while (--s > -1)
    pd[s] = 0;
}
