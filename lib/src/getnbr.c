/*
** getnbr.c for colle4 in /home/boguta_m/rendu/colle_4/srcs
**
** Made by Maxime Boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Wed May 28 17:24:31 2014 Maxime Boguta
** Last update Thu Jun  5 00:26:45 2014 Maxime Boguta
*/

double		do_my_getnbrf(char *s, int state, int i, double post)
{
  double	ret;
  int		div;

  ret = 0.0;
  div = 10;
  while (s[i] && (is_num(s[i]) || s[i] == '.' || s[i] == ','))
    {
      if (state == 1 && (s[i] == '.' || s[i] == ','))
	exit_msg("Error : bad floating expression\n", 1);
      else if (s[i] == '.' || s[i] == ',')
	state = !state;
      else if (!state)
	{
	  ret *= 10;
	  ret += s[i] - 48;
	}
      else
	{
	  post += (double)(s[i] - 48) / div;
	  div *= 10;
	}
      i++;
    }
  return (ret + post);
}

double		my_getnbrf(char *s)
{
  int		state;
  int		i;
  int		neg;

  neg = 0;
  i = -1;
  state = 0;
  if (s && s[++i])
    {
      while (is_num(s[i]) == 0 && s[i])
	{
	  if (s[i] == '-')
	    state = !state;
	  i++;
	}
      if (state)
	return (-do_my_getnbrf(s, 0, i, 0.0));
      else if (!state)
	return (do_my_getnbrf(s, 0, i, 0.0));
    }
  return (0);
}
