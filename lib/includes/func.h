/*
** func.h for my_printf in /home/camill_n/rendu/PSU_2018_my_printf
**
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
**
** Started on  Thu Nov 13 11:44:32 2013 Nicolas Camilli
** Last update Tue Feb 11 14:44:28 2014 camill_n
*/

#ifndef FUNC_H_
# define FUNC_H_
# include <stdarg.h>
# include <stdlib.h>

typedef struct	s_func
{
  char		flag;
  void		(*func_tab)(va_list ap);
}		t_func;

int	get_id_func(const char *str, int *i, t_func **tab);
void	set_flag(int i, char c, t_func **tab, void (*func_tab)(va_list ap));
void	manage_putchar(va_list ap);
void	manage_putstr(va_list ap);
void	manage_put_unsigned(va_list ap);
void	manage_put_nbr(va_list ap);
void	manage_put_octal(va_list ap);
void	manage_put_hexa(va_list ap);
void	manage_put_hexA(va_list ap);
void	manage_put_bin(va_list ap);
void	manage_put_nonimp(va_list ap);
void	manage_put_addrptr(va_list ap);
void	manage_put_long(va_list ap);
int	my_putchar(char c);
void	my_putstr(char *str);
void	my_put_octal(unsigned int nb);
void	my_put_bin(unsigned int nb);
void	my_put_nbr(int nb);
void	my_put_long(long nb);
void	my_put_hexa(unsigned int nb, int mode);
void	my_put_unsigned(unsigned int nb);
void	my_put_nonimp(char *str);
void	set_func(t_func **tab);
int	my_str_wt_len(char **str);
void	my_printf(const char *str, ...);
int	my_getnbr(char *str);
int	my_strcmp(char *s1, char *s2);
char	*my_revstr(char *str);
char	*my_strdup(char *str);

#endif
