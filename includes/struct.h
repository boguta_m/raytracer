/*
** new.h for rt in /home/bousca_a/rendu/parser_rtv
**
** Made by Antonin Bouscarel
** Login   <bousca_a@epitech.net>
**
** Started on  Wed May 21 15:52:51 2014 Antonin Bouscarel
** Last update Wed Jun 11 16:14:14 2014 Maxime Boguta
** Last update Sat Jun  7 05:11:26 2014 camill_n
*/

#ifndef STRUCT_H_
# define STRUCT_H_

# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <math.h>
# include "mlx.h"
# include "errors.h"
# include "global.h"
# include "equat4.h"
# include "network.h"
# include "flags.h"

# define MAX_BOUNCE	4
# define QUATRO(x)	((x) * (x) * (x) * (x))
# define CUBE(x)	((x) * (x) * (x))
# define CARRE(x)	((x) * (x))
# define MLX		all->mlx
# define IMG		all->img
# define WIDTH		1920
# define HEIGHT		1080
# define DIMG		1200

# define BUFF_SIZE	16384

# define RAD_MAGIC(x)	((x) * 0.017453292519943295769236907684886)

# define SPHERE		1
# define PLAN		2
# define CYLINDRE	3
# define CONE		4
# define TORE		5
# define CUBE_T		6

# define OUTPATH	all->info.outimg

# define XEYE		tmp->ray.start.x
# define YEYE		tmp->ray.start.y
# define ZEYE		tmp->ray.start.z
# define VX		tmp->ray.dir.x
# define VY		tmp->ray.dir.y
# define VZ		tmp->ray.dir.z

# define CONST_CONE	tmp->cur->cons

typedef struct s_all	t_all;

typedef struct		s_mlx
{
  void			*mlx_ptr;
  void			*win_ptr;
}			t_mlx;

typedef struct		s_img
{
  void			*img_ptr;
  unsigned char		*data;
  int			size_line;
  int			bpp;
  int			endian;
  int			width;
  int			heigh;
}			t_img;

typedef struct		s_color
{
  double		r;
  double		g;
  double		b;
  double		alpha;
}			t_color;

typedef struct		s_obj
{
  int			type;
  double		pos[3];
  double		rpos[3];
  t_color		color;
  double		size;
  double		size2;
  int			opacity;
  int			special;
  int			shine;
  int			reflex;
  int			specular;
  double		cons;
  char			*path_text;
  t_img			texture;
  struct s_obj		*next;
}			t_obj;

typedef struct		s_light
{
  int			soft;
  int			type;
  int			shape;
  int			size;
  double		pos[3];
  t_color		color;
  double		strengh;
  struct s_light	*next;
}			t_light;

typedef struct		s_hit
{
  double		hit[3];
  double		sim[3];
  t_obj			*obj;
  t_obj			*fhit;
  t_obj			*shadowfr;
  double		dist;
}			t_hit;

typedef struct		s_vector
{
  double		x;
  double		y;
  double		z;
}			t_vector;

typedef struct		s_ray
{
  t_vector		dir;
  t_vector		start;
  unsigned char		bounce;
}			t_ray;

typedef struct		s_tmp
{
  t_ray			ray;
  t_vector		realvec;
  t_vector		lig;
  t_vector		norm;
  t_obj			*cur;
  t_color		color;
  t_hit			hit;
  double		sca;
  int			cur_px_calc[2];
}			t_tmp;

typedef struct		s_texture
{
  char			name[20];
  t_img			img;
  struct s_texture	*next;
}			t_texture;

typedef struct		s_cam
{
  double		pos[3];
  double		rpos[3];
}			t_cam;

typedef struct		s_entity
{
  t_obj			*obj;
  t_light		*light;
  t_texture		*texture;
  t_cam			cam;
}			t_entity;

typedef struct		s_info
{
  unsigned int		*cpt;
  char			*file;
  unsigned int		flags;
  char			*outimg;
  unsigned int		nb_lights;
  unsigned int		aa;
}			t_info;

typedef struct		s_all_obj
{
  int			type;
  void			(*exec)(t_tmp *tmp, t_obj **obj);
  struct s_all_obj	*next;
}			t_all_obj;

typedef struct		s_all
{
  t_info		info;
  t_mlx			mlx;
  t_img			img;
  t_entity		ent;
  t_all_obj		*all_obj;
  int			thread_s;
  int			thread_free;
  int			thread_interval;
  int			nb_thread;
  t_cluster		cluster;
}			t_all;

void		parse_file(t_all *all);
void		add_obj(t_all *all, char *str);
void		add_light(t_all *all, char *str);
char		*find_content(char *str, char *start, char *end);
char		*get_content(char *rss, char *start, char *end);
void		init_all(t_all *all);
void		hook_event(t_all *all);
void		*calc_scene(void *all);
void		calc_cubet(t_tmp *tmp, t_obj **obj);
void		calc_sphere(t_tmp *tmp, t_obj **obj);
void		calc_plan(t_tmp *tmp, t_obj **obj);
void		calc_cylindre(t_tmp *tmp, t_obj **obj);
void		calc_cone(t_tmp *tmp, t_obj **obj);
void		calc_tore(t_tmp *tmp, t_obj **obj);
double		my_getnbrf(char *s);
void		_color_factor(t_tmp *, t_color *l, t_light *c, double s);
char		**my_wordtab(char *str, char c);
void		*my_realloc(void *p, int ns, int os);
double		equat4_smaller_k(double *indice);
void		amount_disp(t_all *all, int nb_thread);
void		sub_calc(t_all *all, int nb_thread);
double		dot_product(t_vector *a, t_vector *b);
double		_vec_norm(t_vector *v);

#endif
