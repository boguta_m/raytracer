/*
** flags.h for flags in /home/boguta_m/rendu/mul_2013_raytracer
**
** Made by Maxime Boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Sat May 31 23:52:11 2014 Maxime Boguta
** Last update Sun Jun  8 19:11:45 2014 Maxime Boguta
*/

#ifndef _FLAGS_H_
# define _FLAGS_H_

# define AA_OPT		1
# define GEN_OUTPUT	2
# define THREADING_OPT	4

# define NO_SHADOWS	128
# define NO_BRIGHT	256
# define NO_EFFECTS	512
# define SIMPLE_LIGHTS	1024
# define NEGATIVE_O	2048

# define HOST		16
# define CLIENT		32

#endif
