/*
** calc.h for rt in /home/boguta_m/rendu/mul_2013_raytracer/includes
**
** Made by Maxime Boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Tue Jun  3 14:56:24 2014 Maxime Boguta
** Last update Fri Jun  6 02:19:54 2014 Maxime Boguta
*/

#ifndef _CALC_H_
# define _CALC_H_

# include "struct.h"

void	call_obj_func(t_all *main, int type, t_tmp *tmp, t_obj *obj);
void	_brightness(t_all *main, t_light *lig, t_color *col, t_tmp *tmp);
void	_checks(t_all *main, t_tmp *tmp);
void	_checks_sha(t_all *main, t_tmp *tmp);
void	_color_factor(t_tmp *tmp, t_color *col, t_light *lig, double fac);
void	_eye(t_all *all, int x, int y, t_tmp *tmp);
void	_eyeprimary(t_tmp *tmp, int x, int y);
void	_eyerotate(t_all *main, t_tmp *tmp);
void	_factors(t_all *main, t_color *col, t_tmp *tmp);
void	_from_lights(t_all *main, t_color *col, t_tmp *tmp);
void	_get_l(t_hit *hit, t_light *lig, t_vector *bri);
void	_hit_cd(t_all *main, t_tmp *tmp);
void	_match(t_all *main, t_tmp *tmp, t_obj *obj);
void	_move(t_all *main, t_tmp *tmp);
void	_move_sha(t_all *main, t_tmp *tmp);
void	_ncon(t_vector *n, double *i, t_obj *obj);
void	_ncy(t_vector *n, double *i, t_obj *obj);
void	_nplane(t_vector *n, double *i, t_obj *obj);
void	_nsphere(t_vector *n, double *i, t_obj *obj);
void	_rot_all(t_all *main, t_vector *pos, t_vector *rot);
void	_rot_all_inv(t_all *main, t_vector *pos, t_vector *rot);
void	_rotx(t_vector *pos, t_vector *rot);
void	_roty(t_vector *pos, t_vector *rot);
void	_rotz(t_vector *pos, t_vector *rot);
void	_shadows(t_all *main, t_tmp *tmp, t_light *lig);
void	_simple_color(t_tmp *tmp, t_color *col, double fac);
void	_trace(t_all *main, t_color *col, t_tmp *tmp);
void	get_norm(t_vector *n, double *i, t_obj *obj);
void	get_pix(t_all *main, int x, int y, t_color *col);
double	soft_light(t_light *, t_tmp *, t_all *);

#endif
