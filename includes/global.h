/*
** global.h for my_select in /home/camill_n/rendu/PSU_2013_my_select
**
** Made by Nicolas Camilli
** Login   <camill_n@epitech.net>
**
** Started on  Sat Jan 11 00:01:31 2014 Nicolas Camilli
** Last update Sun Jun  8 22:48:27 2014 Maxime Boguta
*/

#ifndef GLOBAL_H_
# define GLOBAL_H_

#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include "func.h"
#include "alloc.h"
#include "complement.h"
#include "errors.h"
#include "init.h"
#include "get_next_line.h"
#include "wordtab.h"
#include "texture.h"

#endif
