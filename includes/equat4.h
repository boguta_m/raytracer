/*
** el_solve_de_la_noche.h for raytracer in /home/camill_n/el_solver_de_la_noche
**
** Made by camill_n
** Login   <camill_n@epitech.net>
**
** Started on  Tue Jun  3 16:48:06 2014 camill_n
** Last update Thu Jun  5 00:36:25 2014 Maxime Boguta
*/

#ifndef EQUAT4_H_
# define EQUAT4_H_

# define C_P		4
# define NB_INDICE      4
# define DELTA          36

typedef enum	e_indice
  {
    A,
    B,
    C,
    D,
    E
  }		t_indice;

typedef enum
  {
    W,
    U,
    DELTA_T,
    Z_D
  }		e_init;

typedef enum
  {
    T,
    R,
    S,
    DECAL
  }		e_part;

double		calcul_solution_smallest(double *solution);

#endif
