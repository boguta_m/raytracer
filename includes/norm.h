/*
** norm.h for rt in /home/boguta_m/rendu/mul_2013_raytracer
**
** Made by Maxime Boguta
** Login   <boguta_m@epitech.net>
**
** Started on  Sat Jun  7 17:54:13 2014 Maxime Boguta
** Last update Sat Jun  7 20:27:09 2014 Maxime Boguta
*/

#ifndef NORM_H_
# define NORM_H_

#include "struct.h"

void	_nsphere(t_vector *n, double *i, t_obj *obj);
void	_nplane(t_vector *n, double *i, t_obj *obj);
void	_ncy(t_vector *n, double *i, t_obj *obj);
void	_ncon(t_vector *n, double *i, t_obj *obj);
void	_ncubet(t_vector *n, double *i, t_obj *obj);
void	_ntore(t_vector *n, double *i, t_obj *obj);

# endif
